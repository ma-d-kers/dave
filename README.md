# Conector "Dave" T para uso con máscaras 

Hay versión con y sin válvula anti-retorno, se recomienda su uso.

Si se quiere usar dos entradas de Oxígeno o un entrada de oxígeno y una bolsa de reservorio, se debe imprimir una de las versiones de conectores Dave.
Si ya tenemos la valvula antiretorno impresa no have falta imprimir la version con la valvula integrada. 
Existen version para bolsas de reservorio con conector 22F o 22M.

<img src="https://i.gyazo.com/1ae029d5d5f63eb128dad88c8565a111.png" height="300"> 

# Fichero para imprimir
VERSION ACTUAL VALIDADA: **v1**

Imprimir el que sea necesario y se adapte a la bolsa de reservorio.

T Con Válvula y Conector Macho (ESTANDARD RECOMENDADO)  [Pieza A](https://gitlab.com/ma-d-kers/dave/-/raw/master/Dave_Conector_T_Con_V%C3%A1lvula_AntiRetorno_V1/T_AntiRetorno_22M_22F_Salida_22M_V1/A_T_AntiRetorno_22M_22F_Salida_22M_V1.stl?inline=false) /  [Pieza B](https://gitlab.com/ma-d-kers/dave/-/raw/master/Dave_Conector_T_Con_V%C3%A1lvula_AntiRetorno_V1/T_AntiRetorno_22M_22F_Salida_22M_V1/B_Antiretorno_V%C3%A1lvula_V1.stl?inline=false)

T Con Válvula y Conector Hembra [Pieza A](https://gitlab.com/ma-d-kers/dave/-/raw/master/Dave_Conector_T_Con_V%C3%A1lvula_AntiRetorno_V1/T_AntiRetorno_22M_22F_Salida_22F_V1/A_T_AntiRetorno_22M_22F_Salida_22F_V1.stl?inline=false)  /  [Pieza B](https://gitlab.com/ma-d-kers/dave/-/raw/master/Dave_Conector_T_Con_V%C3%A1lvula_AntiRetorno_V1/T_AntiRetorno_22M_22F_Salida_22F_V1/B_Antiretorno_V%C3%A1lvula_V1.stl?inline=false)

T Con Conector Macho [Pieza A](https://gitlab.com/ma-d-kers/dave/-/raw/master/Dave_%20Conector_T_V1/T__22M_22F_Salida_22M_V1/T__22M_22F_Salida_22M_V1.stl?inline=false)

T Con Conector Hembra [Pieza A ](https://gitlab.com/ma-d-kers/dave/-/raw/master/Dave_%20Conector_T_V1/T__22M_22F_Salida_22F_V1/T__22M_22F_Salida_22F_V1.stl?inline=false)



## Materiales de impresión
Todas las impresiones se deben de hacer con filamento de PLA (si puede ser sin pigmento mejor), es lo que se ha probado y validado por varios hospitales.
Las resinas no sirven por su toxicidad.

## Montaje y Uso


Imprimir las dos piezas, insertar la membrana en la cruzilla y unir las dos piezas con presión. Usar en la entrada de Oxígeno just antes de la máscara.


#### Modelo

Vista Transveral Modelo Macho:

<img src="https://i.gyazo.com/75ebc821b1ad3e1710320ed1dd8399e1.png" height="200">

Vista Transveral Modelo Hembra:

<img src="https://i.gyazo.com/19995999180792a6e7e9001b202e1880.png" height="200">



## Impresión

Imprimir la cruzilla hacia abajo y la otra pieza en la siguente configuración:

<img src="https://i.gyazo.com/2dc37e1dcd6258f144085a05047e2943.png" height="200">

## Parámetros de impresión

Como norma general y teniendo en cuenta donde se van a usar esas piezas se requiere imprimir con una calidad aceptable, siendo preferible generar menos piezas de buena calidad a muchas piezas de peor calidad que pueden presentar problemas diversos.

Estos son los parámetros de impresión que se están usando para validar las piezas cuando se termina el diseño:

* Filamento: PLA 1.75mm
* Altura de capa: 0.2
* Grosor de pared: 1.6
* Relleno: 80%
* Soportes: No hacen falta

Aunque usar los mismos parámetros debería asegurar repetibilidad y calidad esto puede variar dependiendo de la impresora y el filamento que use cada maker por consiguiente se deberán ajustar para conseguir los resultados óptimos.

Las distintas pruebas han demostrado que usar una balsa de adherencia evita problemas de piezas despegadas de las camas.

## Calibración

### Configurar la expansión horizontal

Es fundamental realizar este proceso antes de comenzar a imprimir piezas porque las distintas configuraciones de impresoras combinadas con el uso de distintas marcas de filamento puede generar desviaciones en la impresión que afecten al ajuste final de la pieza con la máscara. 

Imprimir [Calibre_Charlotte_Reforzada.stl](Calibre_Charlotte_Reforzada.stl)


Es muy importante que utilices estas piezas de calibrado y no otras obtenidas en otro canal porque han sido diseñadas específicamente para corregir una holgura detectada en el modelo STL Charlotte que vas a imprimir.

Tras imprimirlas piezas tendrás que comprobar que el cilindro entre justo en la anilla.

No es necesario que pase completamente. pero si que entre ajustado haciendo algo de presión. Si lo podéis meter como en la foto siguiente, será suficiente:

![](https://gitlab.com/ma-d-kers/charlotte/-/raw/master/images/calibre_xh_1.png)

Si no hay forma de que el cilindro entre dentro de la anilla se debe modificar en el cura el parámetro Expansión horizontal:
* Si el cilindro no entraba en la anilla hay que disminuir el valor que haya (se aceptan valores negativos). Se puede ir bajando de décima de milímetro en décima de milímetro.
* Si el cilindro pasa holgado por la anilla habrá que incrementar el valor.
* Es normal que el valor resulte entre -0.2 y +0.2 (se admiten ajustes más precisos, por ejemplo -0.16)

Cuando se consiga un parámetro que permita entrar el cilindro ajustado dentro de la anilla se mantiene para utilizarlo para imprimir.


